# Useful Perception Tools

**List of useful or interesting tools / papers / links / etc for perception of the environment in the field of autonomous driving**

<!--toc-->

## Contents

* [Autonomous Driving Perception](#autonomous-driving-perception)
* [LiDAR](#lidar)
* [Camera](#camera)
* [Grids](#grids)
* [Semantic Segmentation](#segmantic-segmentation)
* [Tools and Algorithms](#tools-and-algorithms)
* [Sensor Calibration and Synchronisation](#sensor-calibration-and-synchronisation)
* [Communications](#communications)
* [Datasets Simulators and Benchmarks](#datasets-simulators-and-benchmarks)
* [Interesting Results](#interesting-results)

<!--toc_end-->

## Autonomous Driving Perception

[Autopia Program](https://autopia.car.upm-csic.es/) - **Web** - Autopia Program. Solid background on motion planning for different types of structured environments, considering multi-sensor perception, mapping and localization uncertainty under a common framework relying in dynamic occupancy grids.

[Ulm Unversity Autonomus Driving 2021](https://www.youtube.com/watch?v=HOp0aVTWVls&list=PLhB_r-ORvFmKPzxh5awT4jIjIVRDURsGb&index=1&t=91s) - **Video** - Research at Ulm University on autonomous driving in urban scenarios.


## LiDAR

[LiDAR Live Display Comparisons - CES 2022](https://youtu.be/lNh2zp-g804?si=c5nbKsufW-Yb9P1r) - **Video** - Samples of different LiDAR sensors 2022 

[BASELABS Dynamic Grid](https://youtu.be/KAwEtT76_HE?si=ajUzAgIeQzitL9wj) - **Video** - Why use dynamic occupancy grids.


## Camera
[Camera to Bird's Eye View UNetXST](https://youtu.be/w5mbg479dpE?si=4SRAXXgUKzMdc0T1) - **Video + Git + Article** - Technique for converting images from different cameras to a bird's eye view.

[Inverse Perspective Mapping](https://thomasfermi.github.io/Algorithms-for-Automated-Driving/LaneDetection/LaneDetectionOverview.html) - **Blog + Code** - Tutorial for understanding and applying IPM


## Grids
[A Random Finite Set Approach for Dynamic Occupancy Grid Maps with Real-Time Application](https://arxiv.org/abs/1605.02406) - **Article** - Dynamic Occupancy Grid article -> Probability hypothesis density / multi-instance Bernoulli (PHD/MIB). Solid mathematical framework derivation and pseudo-code for parallel implementation

[CUDA - PHD/MIB](https://github.com/TheCodez/dynamic-occupancy-grid-map) - **Git code** - Implementation of ["A Random Finite Set Approach for Dynamic Occupancy Grid Maps with Real-Time Application"](https://arxiv.org/abs/1605.02406)

[Object-Level Semantic and Velocity Feedback for Dynamic Occupancy Grids](https://ieeexplore.ieee.org/document/10125076) - **Article** - Extension of a particle-based DOG to classify road users and improve velocity estimation using object-level feedback.

[Dataset: CMCDOG in KITTI](https://github.com/d451gon/kitti_dogma_dataset) - **Dataset** - Dataset containing dynamic occupancy grid estimations for the KITTI dataset using the CMCDOT algorithm.


## Semantic Segmentation

[RangeNet++](https://paperswithcode.com/paper/rangenet-fast-and-accurate-lidar-semantic) - **Article + Code** - Fast and Accurate LiDAR Semantic Segmentation 2019

[YOLOp](https://github.com/hustvl/YOLOP) - **Article + Code** - You Only Look Once for Panoptic Driving Perception.


## Object detection

[Survey LiDAR-based 3D object detection with DL](https://youtu.be/_oFTKDwsbQ0?si=J1Dc7cXNzaW8oCjb) - **Video + Article** - A comprehensive survey of LIDAR-based 3D object detection methods with Deep learning for autonomous driving


## Tools and Algorithms

[Markov Random Fields image analysis class](https://youtu.be/vRN_j2j-CC4?si=3VNDe2GUQb7TvWTl) - **Video** MRF-based image analysis class in 2013

[Loopy Belief Propagation + Markov Random Field example](https://nghiaho.com/?page_id=1366) - **Blog** -  Well explained blog about MRF and LBP to solve the stereo problem.

[Dempster-Shafer Theory for Classification](https://bennycheung.github.io/dempster-shafer-theory-for-classification) - **Blog** - Well explained blog about DST used for classification. A python example is provided.

## Sensor Calibration and Synchronisation

[velo2cam_calibration](https://github.com/beltransen/velo2cam_calibration) - **Article + Code** - The velo2cam_calibration software implements a state-of-the-art automatic calibration algorithm for pair of sensors composed of LiDAR and camera devices in any possible combination

[Spatio-temporal alignment LiDAR-radar 2016](https://ieeexplore.ieee.org/document/7849494) - **Article** - Work presenting different approaches for spatiotemporal alignment of data from asynchronous sensors for low-level fusion are presented. Focused on radar sensors and grid mapping.

[Spatio-temporal aligmnet LiDAR-camera 2023](https://doi.org/10.1177/03611981231184187) - **Article** - Article presenting a spatio-temporal fusion for LiDAR and Camera (not (enterily) based on learning)

[Webinar Sensor Synchronization](https://youtu.be/QfwZ1riLdkM?si=AjxOHosH5-nordwc) - **Webinar** - Webinar about trigger-based sensor synchronization 


## Communications
[LCM UDP Multicast Setup](https://lcm-proj.github.io/lcm/content/multicast-setup.html) - **Web** 

## Datasets Simulators and Benchmarks

[nuScenes](https://www.nuscenes.org/) - **Dataset** - Large-scale autonomous driving dataset with 3D object annotations, including camera, LiDAR, RADAR, IMU and GPS.

[nuPlan](https://www.nuscenes.org/nuplan) - **Benchmark** - Large-scale planning benchmark for autonomous driving.

[From nuScenes to nuPlan](https://youtu.be/Y4Oa_uxfDh4?si=EILroiEUuteXJOd7) - **Video** - From nuScenes to nuPlan: Benchmarking for Progress in Autonomous Driving

[Waymax](https://waymo.com/intl/es/research/waymax/) - **Simulator** - An Accelerated, Data-Driven Simulator for Large-Scale Autonomous Driving Research

[Multiception](https://datasets.hds.utc.fr/project/12) - **Dataset** - Data-sets recorded using experimental vehicles in cooperative scenarios.

[RoadRunner Matlab](https://es.mathworks.com/products/roadrunner.html) - **Simulator** - Interactive editor for creating and testing 3D autonomous driving scenes.

[Waymo Perception](https://waymo.com/open/data/perception/) - **Dataset** - Large-scale autonomous driving perception dataset with high resolution sensor data, HD-Map and labels for 2,030 scenes.

[Waymo tech study 2019](https://justinkek.medium.com/alphabets-waymo-a-technical-study-on-autonomous-vehicle-tech-c128180ab2c5) - **Blog** - Alphabet’s Waymo: A Technical Study on Autonomous Vehicle Tech


## HDMaps

[FlexMap Fusion](https://github.com/TUMFTM/FlexMap_Fusion) **Article + Code** Offline mapping algorithm that fuses HD Maps with OSM


## Interesting Results

[Autopia Program](https://www.youtube.com/@GrupoAUTOPIA/featured) - **Youtube channel** - Autopia Program youtube channel

[Autopia Program demonstrator in roundabouts 2023](https://youtu.be/Zha-9FbH9A0?si=I8TCtZr8WsGMrh4s) - **Video** - Experiments exhibit some of the CSIC's contributions to the New Control European project, which include handling unexpected situations in roundabouts, performing merging maneuvers in high-traffic scenarios and handling sensoric failures safely.

[Sparse LiDAR-based Perception framework 2022](https://youtu.be/3NTBfI9TonU?si=iyNPQ6ySmCxJni9k) - **Video** - Urban escenarios. LiDAR point cloud segmentation + Dynamic occupancy grid + Road users object-level tracking

[Dynamic Occupancy Grid with Neural Networks 2020](https://youtu.be/zMga1nGbXYM?si=Mcq7Jv3STxZTaSL-) - **Video + Article** - Urban scenarios. LiDAR data. Improvement of particle-based DOG with Recurrent Neural Networks

[Object Detection on Dynamic Occupancy Grid using DL 2018](https://youtu.be/Rr9LOrQMgKA?si=p3HFzN2FJHd6CtEu) - **Video + Article** - Object Detection on Dynamic Occupancy Grid Maps Using Deep Learning and Automatic Label Generation.

[LiDAR point pillars Detection and Tracking 2019](https://youtu.be/sOLCgRkWlWQ?si=0YVAaMTTHgU0wykw) - **Video** - Video example of LiDAR point pillars Detection and Tracking


